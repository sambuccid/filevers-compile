# filevers-compile

This project is a simple aggregation of the tool to compile filevers easily  
so then everyone will use the same compiler, version of the compiler and not previous configuration will be needed to contribute to the project  
The windows build is not currently in this repository, it's possible to build for it and i've created an installer as well, but now the project is discontinued, please read readme file in [filevers](https://gitlab.com/sambuccid/filevers)
